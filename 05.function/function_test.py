#함수 정의
# def 함수이름(매개변수) :
#     함수 기능 코딩
#     return 호출한곳에 전달하는 데이터

def add(a, b) :
    sum =a+b
    return sum

def substract(a, b) :
    return a-b

def multiply(a, b) :
    return a*b

def divde(a, b) :
    return a/b


#함수 호출
#함수이름(매개변수에 전달할 데이터)
print("10 + 20 = {0}".format(add(10,20)))
print("10 - 20 = {0}".format(substract(10,20)))
print("10 * 20 = {0}".format(multiply(10,20)))
print("10 / 20 = {0}".format(divde(10,20)))

#매개변수 미정 - *매개변수 : 호출되는 시점에 매개변수 개수 결정
def add_many(*args) :
    sum = 0
    for i in args :
        sum += i  #sum=sum+i
    return sum

print("1+2+3 = {0}".format(add_many(1,2,3)))
print("1+2+3+4+5+6+7+8+9+10 = {0}".format(add_many(1,2,3,4,5,6,7,8,9,10)))


#local variable & global variable
#local variable : 함수의 매개변수나 함수 내에서 선언한 변수로 유효범위는 함수가 호출되서 return되기 전까지 유효
#global variable : 함수 밖에 선언된 변수로 모든 함수가 공유해서 사용

global_variable = 10

def function1() :
    local_variable = 10
    global global_variable 
    global_variable += 10
    print("local variable : {0}   global_variable : {1}".format(local_variable, global_variable))


def function2() :
    local_variable = 100
    global global_variable 
    global_variable += 10
    print("local variable : {0}   global_variable : {1}".format(local_variable, global_variable))

function1()
function2()

add = lambda a,b : a+b
print("lambda function {0}".format(add(3,4)), type(add))