#사용자 정의 Exception
# 1. 사용자 정의 Exception 클래스 정의(Exception 상속)
# 2. 원하는 조건이 발생됐을 때 Exception 발생 (raise 사용자정의Exception() )
# 3. 사용코드에서  Exception 처리 (try~except~finally)

#1.
class UserException(Exception) :
    def __init__(self, message) :
        self.message = message + "는 사용할 수 없습니다."
    
    def get_message(self) :
        return self.message
    def __str__(self):
        return self.message

#2.
def exception_f(nick) :
    if nick == "바보":
        raise UserException(nick)
    print(nick)

#3.
try :
   exception_f("천사")
   exception_f("바보")  #exception 발생
   exception_f("천재")  #exception 발생 후 실행문 실행 안됨
except UserException as e :
    print(e.get_message())
    print(e) # __str__ 호출
  
