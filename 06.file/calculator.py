import sys

def add(a,b) :
    return a+b

def substract(a,b) :
    return a-b

def multiply(a,b) :
    return a*b

def divide(a,b) :
    return a/b

if len(sys.argv) != 4 :
    print("사용법  calculator.py 숫자 연산자 숫자 ",end='  ')
    print("연산자는 + - x / 중 하나 사용")
    sys.exit()

data1 = int(sys.argv[1])
operator = sys.argv[2]
data2 = int(sys.argv[3])

if operator == '+' :
   print("{0} + {1} = {2} ".format(data1,data2, add(data1, data2)) )
elif operator == '-' :
   print("{0} - {1} = {2} ".format(data1,data2, substract(data1, data2)) )
elif operator == 'x' :
   print("{0} x {1} = {2} ".format(data1,data2, multiply(data1, data2)) )
elif operator =='/' :
   print("{0} / {1} = {2} ".format(data1,data2, divide(data1, data2)))
else :
    print("사용법  calculator.py 숫자 연산자 숫자 ",end='  ')
    print("연산자는 + - x / 중 하나 사용")



