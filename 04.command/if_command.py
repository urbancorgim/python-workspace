#조건문
# if 조건식 : 
#     참일때 실행문
# else :
#     거짓일때 실행문

# if 조건식1 :
#    조건식1이 참일때 실행문
# elif 조건식2 :
#     조건식2가 참일때 실행문
# else :
#     거짓일때 실행문

month=int(input("월을 입력하세요 : "))  #input() = 키보드로 데이터입력  int('숫자'):숫자로된 문자열을 정수형으로 변환

if month==1 or month==3 or month==5 or month==7 or month==8 or month==10 or month==12 :
    print("{0}월은 31일까지 있습니다.".format(month)) 
elif month==2 :
    print("{0}월은 28일까지 있습니다.".format(month)) 
elif month==4 or month==6 or month==9 or month==11 :
    print("{0}월은 30일까지 있습니다.".format(month)) 
else :
    print("{0}월은 존재하지 않습니다. ".format(month)) 

