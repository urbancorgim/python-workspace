class Person :  #클래스이름은 대문자로 시작, 두 단어 이어지는 경우 camel case로 이어지는 첫 글자 대무자  예)HelloWolrd
    def __init__(self, name, age) :
        self.name = name
        self.age =age
    
    def info(self) :
        return "이름 : "+self.name+ "  나이 : "+str(self.age)  


class Student(Person) :
    def __init__(self, name, age, major):
        super().__init__(name, age)
        self.major = major
    #override
    def info(self):
        return super().info()+"  전공 : "+self.major


class Instructor(Person) :
    def __init__(self, name, age, subject):
        super().__init__(name, age)
        self.subject = subject
    
    #override
    def info(self):
        return super().info()+"  과목 : "+self.subject


class Employee(Person) :
    def __init__(self, name, age, department):
        super().__init__(name, age)
        self.departement = department
    
    #override
    def info(self):
        return super().info()+"  부서 : "+self.departement