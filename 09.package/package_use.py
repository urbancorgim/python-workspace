#패키지 : 모듈을 디렉토리로 관리
#패키지의 모듈 참조 : import 패키지명.패키지명.모듈     --> 패키지명.패키지명.모듈.함수() 
#                    from  패키지명.패키지명.모듈 import 모듈의 함수, 변수, 클래스 -->함수()
#                    from 패키지명.패키지명  import 모듈 --> 모듈.함수()


import game.graphic.render
from game.sound.echo import echo_test, run_test
from game.play import run

game.graphic.render.render_test()
echo_test()
run_test()
run.run_test()