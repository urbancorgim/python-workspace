class Person :  #클래스이름은 대문자로 시작, 두 단어 이어지는 경우 camel case로 이어지는 첫 글자 대무자  예)HelloWolrd
    def __init__(self, name, age) :
        self.name = name
        self.age =age
    
    def info(self) :
        return "이름 : "+self.name+ "  나이 : "+str(self.age)  


class Student(Person) :
    def __init__(self, name, age, major):
        super().__init__(name, age)
        self.major = major
    #override
    def info(self):
        return super().info()+"  전공 : "+self.major


class Insturctor(Person) :
    def __init__(self, name, age, subject):
        super().__init__(name, age)
        self.subject = subject
    
    #override
    def info(self):
        return super().info()+"  과목 : "+self.subject


class Employee(Person) :
    def __init__(self, name, age, department):
        super().__init__(name, age)
        self.departement = department
    
    #override
    def info(self):
        return super().info()+"  부서 : "+self.departement


# #학생등록
# s1 = Student("송민주", 31, "조리외식경영")
# s2 = Student("김한비", 24, "러시아어")

# #강사등록
# i1 = Insturctor("최현진", 25, "클라우드")
# i2 = Insturctor("허승일", 28, "AI")

# #직원등록
# e1 = Employee("김현주", 25, "클라우드개발팀")
# e2 = Employee("송현진", 26, "클라우드개발팀")


persons =[Student("송민주", 31, "조리외식경영"), 
          Student("김한비", 24, "러시아어"),
          Insturctor("최현진", 25, "클라우드"),
          Insturctor("허승일", 28, "AI"),
          Employee("김현주", 25, "클라우드개발팀"),
          Employee("송현진", 26, "클라우드개발팀")]
#정보출력
for p in persons :
    print("정보 출력 : {0}".format(p.info())) 
    #virtual method invocation : override된 method 호출

#persons[0] --> p = Student("송민주", 31, "조리외식경영")
#persons[1] ---> p = Student("김한비", 24, "러시아어")
#persons[2] ---> p =Insturctor("최현진", 25, "클라우드")
#persons[3] ---> p =Insturctor("허승일", 28, "AI")
#persons[4] ---> p =Employee("김현주", 25, "클라우드개발팀")
#persons[5] ---> p =Employee("송현진", 26, "클라우드개발팀")