a=[1,2,3, ['a','b','c']]
print("a list 첫번째: {0}  두번째:{1}  세번째: {2} 네번째는 배열:{3}".format(a[0], a[1], a[2], a[3]))
print("네번째 배열의 첫번째:{0}  두번째:{1}  세번째:{2} ".format(a[3][0], a[3][1], a[3][2]))

print("a배열의 길이 : {0} 접근 index는 0~{1}".format(len(a), len(a)-1))

#리스트 값 수정
a[0] = 0
print("a배열의 첫번째 데이터 수정 : {0}".format(a))
#리스트 값 삭제
del a[4:]
print("a배열에  추가된 요소 삭제 : {0}".format(a))


#리스트 함수 사용
#추가 : append
a.append(4)
print("a배열에 4요소 추가 {0}".format(a))
a.append([5,6])
print("a배열에 4요소 추가 {0}".format(a))

#삽입 : insert
a.insert(1, 1)
print("a 배열 두번째 1요소 삽입 : {0}".format(a))

#삭제 : remove
a.remove(0)
print("a배열의 첫번째로 나오는 0 데이터 삭제 {0}".format(a))

#정렬 : sort
array = [1,3,2,5,4]
array.sort()
print("a배열 오름차순 정렬 {0}".format(array))
array.sort(reverse=True)
print("a배열 내림차순 정렬 {0}".format(array))

#뒤집기 : reverse
array.reverse()
print(array)

arr =[1,3,2,5,4]
arr.reverse()
print(arr)

#위치반환 : index
print("arr 3요소의 index : {0}".format(arr.index(3)))  #arr[4, 5, 2, 3, 1]

#요소 출력 : pop
print("arr 마지막 요소 출력 {0}".format(arr.pop()))
