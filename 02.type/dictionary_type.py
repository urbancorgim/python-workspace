#dictionary type :  {key:value, ....}  key -중복되지 않는다

dic ={"name":"유민기", "age":27, "age":26} #중복되는 경우 마지막 데이터를 제외한 데이터 무시
print(dic)  #{'name': '유민기', 'age': 26}

#dictionary data  추가  dictionary변수[key] = value
dic["major"] = "컴공"
print(dic) #{'name': '유민기', 'age': 26, 'major': '컴공'}

dic["major"] = "컴퓨터공학"  #key 중복되면 해당 데이터 수정
print(dic) #{'name': '유민기', 'age': 26, 'major': '컴퓨터공학'}

#dictionary 요소 삭제
del dic["major"]
print(dic)
print("이름: {0}   나이:{1}".format(dic["name"], dic["age"]))

#dictionary key list
print(dic.keys()) #list type으로 keys return
for key in dic.keys() :                                    #key로 value얻기
    print("key : {0},  value : {1}".format(key, dic[key])) # value = dic[key] 
    print("key : {0},  value : {1}".format(key, dic.get(key))) #value = dic.get(key) 

#in 연산자 : dictionary에 key 존재여부를 True 또는 False 리턴
print("name is exist ? {0}".format("name" in dic))
print("major is exist ? {0}".format("major" in dic))


#dictionary (key:value) list
print(dic.items())  #dict_items([('name', '유민기'), ('age', 26)])

#dictionary example
cloud_msa =[
     {"name":"유민기", "age":27, "major":"컴공"}, 
     {"name":"조규원", "age":28, "major":"물리학"},
     {"name":"김건우", "age":27,"major":"컴공"}
]

print(cloud_msa)