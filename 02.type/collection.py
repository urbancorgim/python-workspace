a=[1,2,3,4,5]
b =a  #a의 주소값 b에 복사
print("a : {0}  b:{1}  a주소값 : {2}  b주소값:{3}".format(a,b,id(a), id(b)))
print("a is b ? {0}".format(a is b))

a[0] = 6
print("a :{0}  b:{1}".format(a, b))

c=a[:]  #a의 값이 새로운 c에 복사 c = copy[a]도 c=a[:] 같은 값 복사
print("a : {0}  c:{1}  a주소값 : {2}  c주소값:{3}".format(a,c,id(a), id(c)))
print("a is c ? {0}".format(a is c))

a[0] = 7
print("a :{0}  c:{1}".format(a, c))
