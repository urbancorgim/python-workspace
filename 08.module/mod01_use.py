#모듈 : 독립적인 기능을 위한 변수, 함수, 클래스를 모아 놓은 파일
#다른 모듈의 기능 사용 :  import 모듈이름                  --->모듈이름.함수이름()
#                        from 모듈이름 import 함수리스트  ---> 함수이름()



# import mod01   #현재 디렉토리에서 mod01.py 모듈 import

# print("10 + 20 = {0}".format( mod01.add(10,20) ) )
# print("10 - 20 = {0}".format( mod01.sub(10,20) ) )

# from mod01 import *  #mod01.py모듈의 모든 함수 import
from mod01 import add, sub #mod01.py모듈로부터 add, sub import

# print("mode01_use의 __name__ = {0}".format(__name__) )
print("10 + 20 = {0}".format( add(10,20) ) )
print("10 - 20 = {0}".format( sub(10,20) ) )
