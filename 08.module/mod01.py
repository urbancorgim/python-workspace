def add(a,b):
    return a+b

def sub(a,b) :
    return a-b

#__name__ : 모듈이름 단, 실행모듈일 경우 __name__ == "__main__"
# print("mod01 __name__ = {0}".format(__name__) )

if __name__ == "__main__" :
   print(add(10,20))
   print(sub(10,20))