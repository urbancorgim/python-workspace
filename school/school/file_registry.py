from . import views
from .models import Student, Instructor, Employee

def save_file(persons) :
    #프로그램 종료시 views.py persons list를 파일저장
    save_file = open("list.dat", "w")
    for index, p in enumerate(views.persons) :
        if isinstance(p, Student) :
            save_file.write("1,{0},{1},{2}\n".format(p.id,p.name,p.major))
        elif isinstance(p, Instructor) :
            save_file.write("2,{0},{1},{2}\n".format(p.id,p.name,p.subject))
        elif isinstance(p, Employee):
            save_file.write("3,{0},{1},{2}\n".format(p.id,p.name,p.department)) 
    save_file.close()

def init_data_load() :
    pass #프로그램 시작시 파일읽어서 views.py persons list에 저장