
from .exception import DuplicateError, NotFoundError
from .file_registry import save_file  

persons = []
def register(person) :
   #id 중복 check  - 중복될 경우 DuplicateError(id)
   index = is_exist(person.id) 
   if index > -1 :
       raise DuplicateError(person.id) 
   persons.append(person)

def update(person):
    #id check  - 존재하지 않는 경우 NotFoundError(id)
    index = is_exist(person.id)
    if index == -1 :
        raise NotFoundError(person.id)
    persons[index] = person

def remove(id):
    #id check  - 존재하지 않는 경우 NotFoundError(id)
    index = is_exist(id)
    if index == -1 :
        raise NotFoundError(id)
    persons.pop(index)

def getPerson(id) :
    #id check  - 존재하지 않는 경우 NotFoundError(id)
    index = is_exist(id)
    if index == -1 :
        raise NotFoundError(id)
    return persons[index]

def getAllPersons() :
    return persons

def is_exist(id) :
    for index, person in enumerate(persons) :
        if person.id == id :
            return index
    return -1

def save_list():
    save_file(persons)